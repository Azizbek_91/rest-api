<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/register',[RegisterController::class,'register']);
Route::post('/check-code',[RegisterController::class,'checkCode']);

Route::middleware([\App\Http\Middleware\CourierIsValid::class])->group(function (){

    Route::get('/getAll',[\App\Http\Controllers\Api\OrderController::class,'getAllOrders']);
    Route::get('/getNotifyOrder',[\App\Http\Controllers\Api\OrderController::class,'getNotifyOrder']);
    Route::get('/getCurrentOrders',[\App\Http\Controllers\Api\OrderController::class,'getCurrentOrders']);
    Route::get('/getClosedOrders',[\App\Http\Controllers\Api\OrderController::class,'getClosedOrders']);
    Route::get('/accept/{id}',[\App\Http\Controllers\Api\OrderController::class,'accept'])
        ->where('id', '[0-9]+')
        ->middleware('available');

    Route::get('/test',function (){
        return \App\Payment::find(18)->get_name();
    });


});



Route::get('/clear', function () {
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    \Artisan::call('clear-compiled');
    \Artisan::call('config:cache');
    \Artisan::call('cache:clear');
    dd("Cleared");
});
