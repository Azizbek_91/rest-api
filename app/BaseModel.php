<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function get_name(){
       return \DB::table($this->table_translate)->select('name')
            ->where('object_id','=',$this->id)
            ->where('language_id','=',1)
            ->get();
    }
}
