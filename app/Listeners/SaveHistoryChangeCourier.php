<?php

namespace App\Listeners;

use App\Events\onChangeOrderCourier;
use App\OrderHistory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SaveHistoryChangeCourier
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onChangeOrderCourier  $event
     * @return void
     */
    public function handle(onChangeOrderCourier $event)
    {
        $history                = new OrderHistory();
        $history->order_id      = $event->order->id;
        $history->username      = $event->order->courier->sname;
        $history->data_before   = serialize(["courier_id" => $event->order->getOriginal('courier_id')]);
        $history->data_after    = serialize(["courier_id" => $event->order->courier_id]);
        $history->handler       = 'attributes';
        $history->created       = date("Y-m-d H:i:s");
        $history->save();
    }
}
