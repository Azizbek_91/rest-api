<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const DELIVERY  = 6;
    const CLOSED    = 9;
    const CANCELLED = 10;

    const MAX_COOK_TIME = 600;

    protected $table = 'Order';

    public function restaurant(){

        return $this->hasOne(Restaurant::class,'id','manufacturer_id');

    }

    public function payment(){

        return $this->hasOne(Payment::class,'id','payment_id');

    }

    public function user(){

        return $this->hasOne(User::class,'id','user_id');

    }

    public function courier(){

        return $this->belongsTo(Courier::class);

    }

    public function getDeliveryDistanceByKm()
    {
        return round(floatval($this->delivery_distance) / 1000, 2);
    }


    public function getTotal_price()
    {
        $result = 0;
            if ($this->discount) {
                $sum = $this->discount;
                if ('%' === substr($this->discount, -1, 1))
                    $sum = $result * (int)$this->discount / 100;
                $result -= $sum;
            }

            return $result;
    }

    public function isRestaurantInformedThird()
    {
        return $this->restoran_informed == 3;
    }

    public function getCookTimer()
    {
        $orderTime = self::MAX_COOK_TIME * 60; //в секундах
        $orderCreated = strtotime($this->restoran_informed_time);
        $nowTime = time();

        return ($orderTime + $orderCreated) - $nowTime;
    }


}
