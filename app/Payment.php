<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends BaseModel
{
    protected $table ='StorePaymentMethod';
    protected $table_translate = 'StorePaymentMethodTranslate';
    protected $primaryKey = 'id';

}
