<?php

namespace App\Providers;

use App\Order;
use App\Observers\OrderObserver;
use App\Events\onChangeOrderCourier;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\SaveHistoryChangeCourier;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        onChangeOrderCourier::class => [
          SaveHistoryChangeCourier::class,
        ],
    ];

    protected $observers = [
        Order::class => [OrderObserver::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen(
            onChangeOrderCourier::class,
            [SaveHistoryChangeCourier::class, 'handle']
        );

        parent::boot();

    }
}
