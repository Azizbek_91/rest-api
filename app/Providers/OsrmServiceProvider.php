<?php

namespace App\Providers;

use App\Services\OsrmService;
use Illuminate\Support\ServiceProvider;

class OsrmServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('OsrmService',OsrmService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
