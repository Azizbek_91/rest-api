<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends BaseModel
{
    protected $table ='StoreManufacturer';
    protected $table_translate = 'StoreManufacturerTranslate';
}
