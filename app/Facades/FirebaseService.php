<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FirebaseService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'FirebaseService';
    }

}
