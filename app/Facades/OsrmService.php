<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OsrmService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'OsrmService';
    }

}
