<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table ='OrderHistory';
    public $timestamps = false;

}
