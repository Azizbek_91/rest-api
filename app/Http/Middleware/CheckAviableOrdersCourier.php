<?php

namespace App\Http\Middleware;

use App\Courier;
use Closure;

class CheckAviableOrdersCourier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $courier = Courier::getCurrentCourier();
        if($courier->available_order > $courier->active_orders()->count())
            return $next($request);
        else
            return response()->json('Вы не можете принимать заказы, пока не закроете предыдущие!',415);
    }
}
