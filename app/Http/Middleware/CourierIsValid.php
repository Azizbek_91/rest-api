<?php

namespace App\Http\Middleware;

use App\Courier;
use Closure;

class CourierIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->bearerToken()){
            $coureir = Courier::where('token',$request->bearerToken())->first();
            if($coureir and $coureir->status == Courier::ACTIVE){
                session(['courier'=>$coureir->id]);
                return $next($request);
            }
            else
                return response('Unauthorized.', 401);
        }
        return response('Unauthorized.', 401);
    }
}
