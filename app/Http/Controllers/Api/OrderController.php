<?php

namespace App\Http\Controllers\Api;

use App\Courier;
use App\Events\onChangeOrderCourier;
use App\Facades\FirebaseService;
use App\Facades\OsrmService;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Listeners\SaveHistoryChangeCourier;
use App\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends MainController
{
    public $orderRepository;

    public function __construct()
    {
        $this->orderRepository = app(OrderRepository::class);
    }

    public function getAllOrders(){

        $orders = $this->orderRepository->getAll();
        return $this->success([
                'count'=>$orders->count(),
                'orders'=>OrderResource::collection($orders)
        ]);
    }

    public function getCurrentOrders(){

        $orders = $this->orderRepository->getCurrentOrders();

        return $this->success([
            'count' => count($orders),
            'orders' => $orders
        ]);

    }

    public function getClosedOrders(){

        $orders = $this->orderRepository->getClosedOrders();

        return $this->success([
            'count' => $orders->count(),
            'orders' => $orders
        ]);

    }

    public function accept(Request $request,$id){

        // $res = OsrmService::calculateRoute('69.229511','41.264517','69.244284','41.323588');

        $order = Order::find($id);

        if(!$order)
            return $this->error('Order not found',404);

        $res = $this->orderRepository->setOrderToCourier($order);

        if($res === true)
            return $this->success('Вы приняли этот заказ');
        else
            return $this->error($res);
    }

    public function getNotifyOrder(){

        $res = $this->orderRepository->getNotifyOrder();

        return $this->success($res);
    }






}
