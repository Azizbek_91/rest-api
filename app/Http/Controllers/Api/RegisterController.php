<?php

namespace App\Http\Controllers\Api;

use App\Courier;
use Illuminate\Http\Request;
use App\Repositories\CourierRepository;
use Illuminate\Support\Facades\Validator;

class RegisterController extends MainController
{
    protected $courierRepository;

    public function __construct()
    {
        $this->courierRepository = app(CourierRepository::class);
    }

    public function register(Request $request){

        $validator = Validator::make($request->input(), [
            'phone' => 'required|digits:12|numeric',
        ]);

        $phone = $request->input('phone');

        if(!$this->isPhoneNumber($phone))
            return $this->error('xato format',415);

        if ($validator->fails())
            return  $this->error($validator->errors(),415);

        $courier = Courier::where('phone', $phone)->first();

        if(!$courier)
            return $this->error('Coureir not found',404);

        if($courier->status  != Courier::ACTIVE )
            return $this->error('Coureir is not active',415);

        $res = $this->courierRepository->login($courier);

        if(true === $res)
            return $this->success('SMS code yuborildi!');
        else
            return $this->error('SMS yuborishda xatolik!',$res);

    }

    public function checkCode(Request $request){

        $validator = Validator::make($request->all(), [
            'code' => 'required|digits:4|numeric',
            'phone' => 'required|digits:12|numeric',
        ]);

        if($validator->fails())
            return $this->error($validator->errors(),415);

        $courier = Courier::where('phone', $request->input('phone'))->first();

        if(!$courier)
            return $this->error('Coureir not found',404);

        $res = $this->courierRepository->checkCode($courier,$request->input('code'));
        if(false === $res)
            return $this->error('SMS code incorrect!');
        else
            return $this->success($res);

    }


    public function isPhoneNumber($number)
    {
        $regex = '/(^998)\d{9}/';
        if (preg_match($regex, $number) && strlen($number) === 12) {
            return true;
        } else {
            return false;
        }
    }
}
