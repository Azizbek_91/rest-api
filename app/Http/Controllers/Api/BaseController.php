<?php

namespace App\Http\Controllers\Api;

class MainController extends \App\Http\Controllers\Controller
{
    public function success($res,$status = 200){

        return response()->json(['data'=>$res],$status);

    }

    public function error($res,$status = 500){

        $data['error'] = $res;
        return response()->json(['data'=>$data],$status);

    }


}
