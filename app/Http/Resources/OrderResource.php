<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"                        => $this->id,
            "order_code"                => $this->order_code,
            "delivery_id"               => $this->delivery_id,
            "payment_id"                => $this->payment_id,
            "area_id"                   => $this->area_id,
            "zone_id"                   => $this->zone_id,
            "lat"                       => $this->lat,
            "lon"                       => $this->lon,
            "client_chat_id"            => $this->client_chat_id,
            "telegram_id"               => $this->telegram_id,
            "date"                      => $this->date,
            "time"                      => $this->time,
            "delivery_price"            => $this->delivery_price,
            "delivery_orginal_price"    => $this->delivery_orginal_price,
            "delivery_distance"         => $this->delivery_distance,
            "total_price"               => $this->total_price,
            "price_without_discount"    => $this->price_without_discount,
            "status_id"                 => $this->status_id,
            "paid"                      => $this->paid,
            "paid_by_client"            => $this->paid_by_client,
            "user_name"                 => $this->user_name,
            "user_address"              => $this->user_address,
            "user_home"                 => $this->user_home,
            "user_unit"                 => $this->user_unit,
            "user_floor"                => $this->user_floor,
            "user_apartment"            => $this->user_apartment,
            "delivery_in"               => $this->delivery_in,
            "user_phone"                => $this->user_phone,
            "user_comment"              => $this->user_comment,
            "ip_address"                => $this->ip_address,
            "created"                   => $this->created,
            "updated"                   => $this->updated,
            "started"                   => $this->started,
            "discount"                  => $this->discount,
            "manufacturer_id"           => $this->manufacturer_id,
            "restoran_informed"         => $this->restoran_informed,
            "restoran_avto_informed"    => $this->restoran_avto_informed,
            "restoran_confirm"          => $this->restoran_confirm,
            "restoran_confirm_time"     => $this->restoran_confirm_time,
            "restoran_cancel"           => $this->restoran_cancel,
            "restoran_cancel_time"      => $this->restoran_cancel_time,
            "restoran_final_confirm"    => $this->restoran_final_confirm,
            "restoran_informed_time"    => $this->restoran_informed_time,
            "operator_id"               => $this->operator_id,
            "courier_id"                => $this->courier_id,
            "courier_type"              => $this->courier_type,
            "delivery_time"             => $this->delivery_time,
            "order_from"                => $this->order_from,
            "who_paid"                  => $this->who_paid,
            "tracking"                  => $this->tracking,
            "order_with_time"           => $this->order_with_time,
            "give_courier"              => $this->give_courier,
            "courier_come"              => $this->courier_come,
            "courier_come_time"         => $this->courier_come_time,
            "dont_call"                 => $this->dont_call,
            "is_ready"                  => $this->is_ready,
            "position_order_courier"    => $this->position_order_courier,
            "delivery_discount_from_man"=> $this->delivery_discount_from_man,
            "is_ready_time"             => $this->is_ready_time,
            "guilt_manufacturer"        => $this->guilt_manufacturer,
            "delivery_starting"         => $this->delivery_starting,
            "courier_entering_to_res"   => $this->courier_entering_to_res,
            "courier_entering_to_client"=> $this->courier_entering_to_client,
            "pay_status"                => $this->pay_status,
            "delivery_price_payed"      => $this->delivery_price_payed,
            "extra"                     => $this->extra
        ];


    }
}
