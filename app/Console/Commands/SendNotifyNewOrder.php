<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class SendNotifyNewOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check orders for courier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::where('status_id',Order::DELIVERY)->get();
        foreach ($orders as $order)
            echo $order->id."\n";
        return 0;
    }
}
