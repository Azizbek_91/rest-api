<?php

namespace App\Services;
use GuzzleHttp\Client;
use function Symfony\Component\Translation\t;

class FirebaseService
{
    public $client;

    public function __construct()
    {
        $this->client = app(Client::class);
    }

    public function send($tokens){

        $fields = array
        (
            'registration_ids' => $tokens,
            'data'=>$this->getMessage(),
            "time_to_live" => 30
        );

        $headers = array
        (
            'Authorization: key=' .env('FCM_KEY'),
            'Content-Type: application/json'
        );


        $res = $this->client->request('POST',env('FCM_URL'),[
            'headers'=>$headers,
            'form_params'=>$fields,
            'curl' => array( CURLOPT_SSL_VERIFYPEER => false ),

        ]);

        if($res->getStatusCode() == 200){

            return true;
        }

        else{
            $this->_error_code = $res->getStatusCode();
            return false;
        }


    }

    private function getMessage()
    {
        $arr = array();

        $arr['msg']['title']            = 'Новый Заказ!' ;
        $arr['msg']['vibrate']          = 1;
        $arr['msg']['sound']            = 1;

        return $arr;
    }

}
