<?php

namespace App\Services;

class OsrmService
{
    //http://127.0.0.1:5000/route/v1/driving/69.229511,41.264517;69.244284,41.323588
    public function calculateRoute($curLng, $curLat, $manLng , $manLat)
    {
        $rest = [];

        $finishUrl = env('OSRM_URL').''.$curLng.','.$curLat.';'.$manLng.','.$manLat;

        $results = file_get_contents($finishUrl);

        $decode = json_decode($results, JSON_UNESCAPED_UNICODE);

        $rest['time'] = $decode['routes']['0']['legs']['0']['duration'];

        $rest['km'] = $decode['routes']['0']['legs']['0']['distance'] / 1000;

        return $rest;
    }

}
