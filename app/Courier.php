<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Courier extends Model
{
    public $timestamps = false;
    const ACTIVE = 2;
    protected $table = 'OrderCourier';

    public static function getCurrentCourier()
    {
        return self::findOrFail(session('courier'));
    }


    public function orders(){

        return $this->hasMany(Order::class);

    }

    public function active_orders(){

        return $this->orders()->whereNotIn('status_id',[Order::CLOSED,Order::CANCELLED]);

    }

    public function closed_orders(){

        return $this->orders()->whereIn('status_id',[Order::CLOSED]);

    }

    public function cancelled_orders(){

        return $this->orders()->whereIn('status_id',[Order::CANCELLED]);

    }
}
