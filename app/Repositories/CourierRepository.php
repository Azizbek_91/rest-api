<?php

namespace App\Repositories;

use App\Courier;
use App\Facades\SmsService;

class CourierRepository implements Repository
{
    protected $courier;

    public function login(Courier $courier){

        $courier->check_code = rand(1000,9999);

        $courier->save();

        $text = 'Code: '.$courier->check_code;

        $res = SmsService::send($courier->phone,$text);

        if($res == false)
            return SmsService::getError();
        else
            return true;

    }

    public function checkCode(Courier $courier ,int $code) {

        $this->courier = $courier;

        if($this->courier->check_code === $code){

            $this->courier->token = \Str::random(32);

            $this->courier->save();

            return [
                'id'        =>$this->courier->id,
                'name'      =>$this->courier->name,
                'car_number'=>$this->courier->car_number,
                'phone'     =>$this->courier->phone,
                'token'     =>$this->courier->token
            ];
        }
        return false;
    }


}
