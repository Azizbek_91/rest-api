<?php

namespace App\Repositories;

use App\Order;
use App\Courier;
use App\Facades\OsrmService;
use Illuminate\Support\Facades\DB;
use App\Events\onChangeOrderCourier;

class OrderRepository implements Repository
{

    public function getCourier()
    {
        return Courier::getCurrentCourier();

    }

    public function getAll(){

        return $this->getCourier()->orders;
    }

    public function getCurrentOrders(){

        return $this->getOrders(
            $this->getCourier()->active_orders
        );

    }

    public function getClosedOrders(){

        return  $this->getCourier()->closed_orders;
    }

    public function setOrderToCourier(Order $order){

        $courier = $this->getCourier();
        DB::beginTransaction();

            if($order->courier_id > 0){
                DB::rollBack();
                return 'Заказ уже занят';
            }

            $order->courier_id = $courier->id;
            $order->save();

            if($courier->id !== $order->courier_id){
                DB::rollBack();
                return 'Заказ уже занят';
            }

        DB::commit();

        return true;

    }

    private function getOrders($orders)
    {
        $orderList = array ();

        foreach ($orders as $order) {

            $item = array (
                "id" => $order->id,
                "order_code" => $order->order_code,
                "restaurant" => $order->restaurant->get_name(),
                "mlatitude" =>  $order->restaurant->lat,
                "mlongitude" => $order->restaurant->lon,
                "payment_id" => $order->payment_id,
                "payment_type" => $order->payment->get_name(),
                "delivery_price" => is_null($order->delivery_price) ? 0 : $order->delivery_price,
                "delivery_diff" => $order->delievry_discount_from_man,
                "delivery_discount_bringo"=> $order->delievry_discount_from_bringo,
                "total_price" => $order->getTotal_price(),
                "name" => $order->user_name,
                "courier_times" => [
                    "created_time" => $order->created,
                    "max_cooking_time" => ($order->isRestaurantInformedThird()) ? $order->getCookTimer() : Order::MAX_COOK_TIME*60,
                    "start_timer" => $order->isRestaurantInformedThird(),
                ],
                "latitude" => $order->lat,
                "longitude" => $order->lon,
                "status" => $order->status_id,
                "address" => $order->address,
                "address2" => $order->user_address2,
                "distance" => $this->distanceCalculation($order->restaurant->lat, $order->restaurant->lon, $order->lat, $order->lon),
                "user_comment" => $order->user_comment,
                "admin_comment" => $order->admin_comment,
                "comment" => $order->user_comment,
                "courier_come" => $order->courier_come,
            );

            $item["user_phone"] = $order->user_phone;
            $item["user_phone2"] = $order->user_phone2;

            array_push($orderList, $item);
        }

        return $orderList;
    }

    public function getNotifyOrder()
    {
        $courierId = Courier::getCurrentCourier()->id;

        $groups = $this->getGroupManufacturersAndCouriers($courierId);

        $orders = $this->getReadyOrders();

        $result = [];

        if(empty($orders))
            return [];

        foreach ($orders as $item) {

            $distance = $this->getDistanceCourierToManufacturer($item->m_lat, $item->m_lng, $item->c_lat, $item->c_lng);
            $man_groups = $this->getManufacturerGroups($item->manufacturer_id);
            $check_order_for_groups = $this->checkForGroups($man_groups,$groups, $item, $distance);

            if (true === $check_order_for_groups)
                $result[] = $item;

            if ($this->checkForAll($check_order_for_groups,$groups, $man_groups))
                $result[] = $item;

            $item->distance = $this->distanceCalculation($item->c_lat, $item->c_lng, $item->m_lat, $item->m_lng);

        }

        return $this->sortByDistance($result);
    }

    private function getGroupManufacturersAndCouriers($courierId)
    {
        $getGroupManufacturer = "SELECT
                                    gm.group_id ,
                                    gm.manufacturer_id,
                                    gc.group_id,
                                    gc.courier_id,
                                    g.*

                                FROM
                                    groups_manufacturer as gm,
                                    groups_courier as gc,
                                    groups as g
                                WHERE
                                    gm.group_id = gc.group_id
                                    AND gc.courier_id = $courierId
                                    AND g.id = gc.group_id
                                    AND g.id = gm.group_id
                                    AND g.show_all = 0";

        return \DB::select($getGroupManufacturer);
    }

    private function getReadyOrders()
    {

        $sql = 'SELECT  "order_id",
                        "order_code",
                        "address",
                        "address2",
                        "home",
                        "unit",
                        "floor",
                        "apartment",
                        "user_comment",
                        "admin_comment",
                        "res_address",
                        "manufacturer_id",
                        "res_name" as "name",
                        "distance",
                        "delivery",
                        "delivery_orginal",
                        "delivery_diff",
                        "is_discount_bringo",
                        "product_price",
                        "total_price",
                        "payment_id",
                        "payment_type",
                        "delivery_id",
                        "delivery_type",
                        "resent",
                        "max_cooking_time",
                        "c_lat",
                        "c_lng",
                        "m_lat",
                        "m_lng",
                        "latitude",
                        "longitude",
                        "distance_courier",
                        "time",
                        "date",
                        "set_courier_date"
                   FROM "push_orders" WHERE status = 0';

        return \DB::select($sql);
    }

    private function getDistanceCourierToManufacturer($latFrom,$lonFrom ,$latTo,$lonTo){

        $latFrom = deg2rad($latFrom);
        $lonFrom = deg2rad($lonFrom);
        $latTo = deg2rad($latTo);
        $lonTo = deg2rad($lonTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * 6371000;

    }

    private function checkForAll($check_order_for_groups,$group_courier,$groups):bool
    {
        /*show order for all couriers*/
        if(true === $check_order_for_groups)
            return true;

        return  (empty($groups) and empty($group_courier));
    }

    private function sortByDistance(array $result):array
    {
        $arr = [];
        foreach ($result as $item){
            $arr[] = (array)$item;
        }

        usort($arr, fn($a, $b) => $a['distance'] <=> $b['distance']);
        return $arr;
    }
    private function getManufacturerGroups($manufacturer)
    {
        $sql = "SELECT g.* , m.*
                        FROM groups g, groups_manufacturer m
                        WHERE m.manufacturer_id = $manufacturer
                        AND g.id = m.group_id AND g.show_all = 0";
        return DB::select($sql);
    }

    private function checkForGroups(array $man_groups,array $groups, object $item, float $distance)
    {
        $max_interval_time =  max(explode('-', implode('-', array_column($man_groups, 'interval_group'))));
        $distance_max      =  max(explode('-', implode('-', array_column($man_groups, 'distance'))));

        /*if distance is large*/
        if(($distance / 1000) > (float)$distance_max and !empty($man_groups))
            return true;

        foreach ($groups as $one) {

            $dist = explode('-', $one->distance);
            $interval_time = explode('-', $one->interval_group);
            $distance = round($distance / 1000, 1);
            $bool_distance = (min($dist) <= $distance && max($dist) >= $distance);
            $check_time = ((strtotime("now") >= ((int)strtotime($item->date) + (int)$interval_time[0]))
                and (strtotime("now") <= ((int)strtotime($item->date) + (int)$interval_time[1])));

            $eq = $one->manufacturer_id == $item->manufacturer_id;

            if(empty($man_groups) and $one->show_all_orders ==1)
                return true;

            if ($eq and ($bool_distance and $check_time))
                return true;

        }

        return ((!empty($max_interval_time) and strtotime("now")
            > (int)(strtotime($item->date) + (int)$max_interval_time))) ? 'show' : false;

    }

    private function distanceCalculation($lat, $lon, $lat1, $lon1)
    {
        return OsrmService::calculateRoute($lon,$lat,$lon1,$lat1)['km'];
    }


}
